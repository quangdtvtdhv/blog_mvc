
<!--hoàng xem giúp anh . làm thế này đã đúng chưa .-->
<?php
include "../config.php";

class User

{
    public $table = 'users';

    public $db;

    public function __construct()
    {
        $this->db = include"../config.php";
        $this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION)
    }

    public $id;
    public $username;
    public $password;
    public $email;
    public $role;
    public $created_at;
    public $updated_at;

    public function read($id, $username, $password, $email, $role, $created_at, $updated_at )
    {
        $stmt = $this->db->prepare("SELECT * FROM $this->table WHERE id=:id");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':created_at', $created_at);
        $stmt->bindParam(':updated_at', $updated_at);

        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }

    public function update($username, $password, $email, $role, $updated_at)
    {
        $stmt = $this->db->prepare("INSERT * FROM $this->table WHERE id=:id");
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':updated_at', $updated_at);

        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);


    }

    puclic function delete($id, $username, $password, $email, $role, $created_at, $updated_at)
    {
        $stmt = $this->db->prepare("DELETE * FROM $this->table WHERE id=:id");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':created_at', $created_at);
        $stmt->bindParam(':updated_at', $updated_at);

        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }

    public function save($id, $username, $password, $email, $role, $created_at, $updated_at)
    {
        $stmt = $this->db->prepare("INSERT * FROM $this->table WHERE id=:id");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':created_at', $created_at);
        $stmt->bindParam(':updated_at', $updated_at);

        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }

    public function setPassword($password)
    {
        $this->password=md5($password);
    }

}