<?php
    include ('controller/c_user.php');
    class View{
        public function print($string)
        {
            echo "$string";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin Panel</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<!--    <div id="wrapper">-->
<!--        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">-->
<!--            <div class="navbar-header">-->
<!--                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">-->
<!--                    <span class="sr-only">Toggle navigation</span>-->
<!--                    <span class="icon-bar"></span>-->
<!--                    <span class="icon-bar"></span>-->
<!--                    <span class="icon-bar"></span>-->
<!--                </button>-->
<!--                <a class="navbar-brand" href="index.html">Personal Blog Admin</a>-->
<!--            </div>-->
<!--            <!-- /.navbar-header -->-->
<!---->
<!--            <ul class="nav navbar-top-links navbar-right">-->
<!--                <!-- /.dropdown -->-->
<!--                <li class="dropdown">-->
<!--                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">-->
<!--                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>-->
<!--                    </a>-->
<!--                    <ul class="dropdown-menu dropdown-user">-->
<!--                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>-->
<!--                        </li>-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                    <!-- /.dropdown-user -->-->
<!--                </li>-->
<!--                <!-- /.dropdown -->-->
<!--            </ul>-->
<!--            <!-- /.navbar-top-links -->-->
<!--            <div class="navbar-default sidebar" role="navigation">-->
<!--                <div class="sidebar-nav navbar-collapse">-->
<!--                    <ul class="nav" id="side-menu">-->
<!--                        <li class="sidebar-search">-->
<!--                            <div class="input-group custom-search-form">-->
<!--                                <input type="text" class="form-control" placeholder="Search...">-->
<!--                                <span class="input-group-btn">-->
<!--                                    <button class="btn btn-default" type="button">-->
<!--                                        <i class="fa fa-search"></i>-->
<!--                                    </button>-->
<!--                                </span>-->
<!--                            </div>-->
<!--                            <!-- /input-group -->-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#"><i class="fa fa-book fa-fw"></i> Posts<span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-second-level">-->
<!--                                <li>-->
<!--                                    <a href="">List Post</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="">Add Post</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="">List Tags</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#"><i class="fa fa-group fa-fw"></i> Categories<span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-second-level">-->
<!--                                <li>-->
<!--                                    <a href="">List Category</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="">Add Category</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-second-level">-->
<!--                                <li>-->
<!--                                    <a href="">List User</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="">Add User</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                            <!-- /.nav-second-level -->-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#"><i class="fa fa-comment fa-fw"></i> Comments</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <!-- /.sidebar-collapse -->-->
<!--            </div>-->
<!--            <!-- /.navbar-static-side -->-->
<!--        </nav>-->
<!--        <div id="page-wrapper">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-12">-->
<!--                    <h1 class="page-header">Tables</h1>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-lg-12">-->
<!--                    <div class="panel panel-default">-->
<!--                        <div class="panel-heading">-->
<!--Kitchen Sink-->
<!--</div>-->
<!--                        <div class="panel-body">-->
<!--                            <div class="table-responsive">-->
<!--                                <table class="table table-striped table-bordered table-hover">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th>#</th>-->
<!--                                        <th>First Name</th>-->
<!--                                        <th>Last Name</th>-->
<!--                                        <th>Username</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>1</td>-->
<!--                                        <td>Mark</td>-->
<!--                                        <td>Otto</td>-->
<!--                                        <td>@mdo</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>2</td>-->
<!--                                        <td>Jacob</td>-->
<!--                                        <td>Thornton</td>-->
<!--                                        <td>@fat</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>3</td>-->
<!--                                        <td>Larry</td>-->
<!--                                        <td>the Bird</td>-->
<!--                                        <td>@twitter</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<?php
if (!isset($_SESSION['admin'])) {
header('Location: login.php');
exit;
}



// Lấy ra danh sách User từ DB sử dụng FETCH_ASSOC -> sẽ trả lại dạng mảng User.
$listUser = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (isset($_GET['delete'])) {
$stmt = $db->prepare("DELETE FROM users WHERE id=:id");
$stmt->bindParam(':id', $_GET['delete']);
$stmt->execute();

header('Location: list_user.php');
}

?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List User</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Blog User
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">Username</th>
                                <th width="25%">Email</th>
                                <th width="10%">Role</th>
                                <th width="5%">Edit</th>
                                <th width="5%">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($listUser as $user) :?>
                                <tr>
                                    <td><?=$user['id']; ?></td>
                                    <td><?=$user['username']; ?></td>
                                    <td><?=$user['email']; ?></td>
                                    <td><?php if ($user['role'] == 1) echo 'Admin'; else echo 'Editor'; ?></td>
                                    <td><a href="update_user.php?id=<?=$user['id'];?>" class="btn btn-primary">Edit</a></td>
                                    <td><a href="list_user.php?delete=<?=$user['id'];?>" class="btn btn-danger">Delete</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="add_user.php" class="btn btn-warning">Add User</a>
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/3.0.3/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
</body>
</html>
